defmodule Omdb.Base do
  defmacro __using__(_) do
    quote do
      use HTTPoison.Base

      defp process_response_body(body) do
        body
        |> Poison.decode!
      end

      defp process_url(url) do
        api_key = Application.fetch_env!(:omdb, :api_key)
        "https://www.omdbapi.com/?" <> url <> "&apikey=#{api_key}"
      end
    end
  end
end