defmodule Omdb.Movie do
  use Omdb.Base

  def findByID(id) do
    get!("i=#{id}&type=movie").body
  end
end