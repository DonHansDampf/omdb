defmodule Omdb do
  use Omdb.Base

  def configuration do
    get!("configuration?").body
  end
end
