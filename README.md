# Omdb

Simple OMDB API Client.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `omdb` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:omdb, "~> 0.1.0"}
  ]
end
```

### config/config.exs

```
import Config

config :omdb,
  api_key: "XXXXXX"
```

Find Movies by IMDB-ID:

```
Omdb.Movie.findByID('tt0325703')
```

---

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/omdb](https://hexdocs.pm/omdb).

